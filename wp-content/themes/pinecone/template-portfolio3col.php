<?php
/**
 * Template Name: Portfolio 3 columns
 *
 * Iya:) Custom portfolio template.
 */
get_header();


    if(function_exists('get_field')){
            $seoBlock = get_field('seo_block');
    }


if (get_post_meta($post->ID, 'display_title', true) != "off") { pinecone_title(); } ?>
<?php
$columns = 'one-third column';
$filters_array = (get_post_meta($post->ID, 'portfolio_filters', true)) ? get_post_meta($post->ID, 'portfolio_filters', true) : '';

get_template_part('content-filter');
get_template_part('content-masonry');
?>

<br>
<br>
<div class="container container-content">

<p>Лазерный луч считается научным открытием, которое послужило техническому прогрессу в начале прошлого века. Изначально лазер использовался в медицине, промышленности, вооружении и научной сфере. В наше время, новейшие технологии и качественное оборудование позволяют выполнять лазерную гравировку на дереве. Такой вид лазерной резки становится настоящим изобретением искусства и воплощает в реальность даже самый сложный дизайн. 
</p>
</div>
 <a href="" class="spoiler_links"><p class="" align="center">читать далее</p></a>
 <div class="spoiler_body" class="sixteen columns" class="container container-content">
 <style>
   .qw {
    border: 2px solid #ebebeb;
    padding: 2px;
   }
  </style> 
 <style type="text/css">
	font-size:16px;
	font-weight:300;
	line-height:30px;
	text-align:left;
  </style>
  <div class="container container-content">
	<div class="sixteen columns">
<p>Мы — молодая креативная студия и мощная производственная компания. Лазерное выжигание по дереву – наше основное занятие, которое помогает выполнять услуги по резке материалов, а также производить широкую линейку товаров для декорирования дома и офиса. Красивая, качественная, любого вида гравировка по дереву от нашей студии, позволяет из обычных деревянных предметов создавать декор интерьера, памятные подарки и сувениры. Ведь такой способ, как лазерная гравировка дерева, делает вещь оригинальной и абсолютно уникальной. Мы с точностью создаем рисунок, надпись, узор любой сложности, с учетом всех деталей и тонкостей.</p>
<h5>Основные преимущества гравировки на дереве:</h5>

 	<p>1. Срок изделия. Гравировка не изнашивается и не портится при любых обстоятельствах.</p>
	<p>2. Качество. Минимальное воздействие на площадь поверхности не портит материал и позволяет идентично повторять изначальный макет.</p>
 	<p>3. Скорость. Благодаря современному лазерному станку по дереву заказ любой сложности выполняется быстро и качественно.</p>
 	<p>4. Универсальность. Нет физического воздействия на материал, поэтому работа может выполняться даже на самых хрупких и труднодоступных местах.</p>
 	<p>5. Практичность. Индивидуальные макеты и уникальные размеры при лазерной резке дерева выполнят любую задачу и с точностью воплотят задумку заказчика.</p>
 	<p>6. Экономия времени и средств. При выполнении заказа, Вам не нужно создавать клише, для гравировки по дереву.</p>
 	<p>7. Достаточно лишь предоставить нам изображение. Команда опытных и творческих специалистов доведут дело до конца и оформят его в лучшем виде.</p>

<h5>Лазерная резка по дереву для клиентов из любой сферы</h5>

<p>На постоянной основе сотрудничаем с инженерами, дизайнерами, рекламными агентствами, дизайнерами одежды, частными лицами и организациями. Благодаря современной лазерной резке дерева, помогаем проектировщикам и креативным людям воплотить любую идею и задумку быстро и доступно в пределах Киева и Украины. На нашем опыте насчитывается немало проектов, где гравировка на дереве Киев помогла удовлетворить немало запросов и создать большое количество красоты для дома и офисов. Мы помогаем быть оригинальными, творческими и уникальными.</p>

<p>Лазерное выжигание по дереву высшего качества</p>

<p>Главную роль качественной работы выполняет наше оборудование. А именно: THUNDER LASER NOVA63</p>

<p>Современное оборудование высшего качества лазерный резак по дереву NOVA обладает всеми лучшими функциями, такими как серводвигатель и отличное качество гравировки с высоким разрешением.</p>

<h5>Возможные применения такого оборудования:</h5>

 	<p>1. Электронная резка дерева;</p>
 	<p>2. Деревообработка и резка;</p>
 	<p>3. Мрамор и камень;</p>
 	<p>4. Травление стекла;</p>
 	<p>5. Корпоративные распродажи;</p>
 	<p>6. Свадебные памятные вещи;</p>
 	<p>7. Различные наборы табличек;</p>
 	<p>8. Гравировка на акриловой плоскости;</p>
 	<p>9. Резка по дереву на вывесках;</p>

<p>Вы даже не представляете насколько широкие наши возможности, и какой большой спектр услуг по созданию лучших и самых оригинальных предметов мы можем предложить. И так, лазерный станок для резки дерева сможет Вам создать: аппликации, игрушки и игры, деревянные модели, фотоальбомы, праздничные украшения, лазерные вырезы и приглашения, корпоративные и спортивные награды, акриловые бляшки, рамка для фотографий, единственные в своем роде подарки, гравированные зеркала. Также, резка по дереву создает архитектурные модели, пользовательские теги для животных, инкрустированный вывесок, 3D-модели, вышитые джинсовые джинсы, гравировка фотографий, гравировка штрих-кода, логотип, гравировка на частях. Благодаря гравировке по дереву Киев выполняется идентификация инструмента, маркировка медицинской части, гравировка на ноутбуке и MP3-плеере.</p>

<p>Не только дерево, но и почти все неметаллические материалы и некоторые металлические могут быть разрезаны с помощью лазера ЧПУ.</p>

<h5>Выполнить резку сможем для таких материалов:</h5>

 <p>1. Лазер по дереву</p>
 <p>2. Фанера</p>
 <p>3. МДФ</p>
 <p>4. Картон</p>
 <p>5. Древесный шпон</p>

<h5>Индивидуальный подход к каждому клиенту</h5>

<p>Обращаясь в нашу креативную команду, Вы убедитесь, что лазерная резка дерева Киев высшего качества только у нас. Благодаря опыту и усиленному вниманию ко всем деталям, мы выработали четкий алгоритм работы с клиентом, который позволяет выполнять даже самые сложные задачи. Теперь лазерная гравировка по дереву в Киеве доступна для всех творческих людей, которые хотят украсить и разнообразить свою жизнь. Ведь, все заключается в деталях и в отношении к ним.</p>

<h5>С нами Вы получаете:</h5>

 <p>1. Выполнение заказа в самые короткие сроки;</p>
 <p>2. Материалы лазерной резки дерева Киев высшего качества;</p>
 <p>3. Приятный прайс и выгодные предложения;</p>
 <p>4. Прием и выполнение заказа в режиме онлайн;</p>
 <p>5. Индивидуальное изготовление эскизов;</p>
 <p>5. Доставку в любую точку страны.</p>


<h5>Как заказать лазерную гравировку по дереву в Киеве</h5>

<p>Когда приходит время внести в свою консервативную жизнь и обычные предметы нотку оригинальности, творчества и уникальности обязательно обращайтесь именно в нашу компанию. Ведь именно у нас так легко и качественно предоставляется лазерная гравировка по дереву Киев.</p>

<p>Чтобы полностью осуществить свою мечту, для начала Вам нужно с точностью определить, на каком предмете и с какими параметрами хотите выполнить гравировку на дереве Киев. Далее следует важный вопрос, какой это будет материал и в каких целях он будет использоваться. Составив общую картину для гравировки на дереве Киев, Вы смело можете обращаться к нам с помощью контактных данных, которые указаны на сайте.</p>

<p>Широкий спектр возможностей компании, позволяет предоставлять клиентам на лазерную гравировку по дереву, цену лучшую на рынке.</p>

<p>С нами приятно сотрудничать. Опытная команда дизайнеров и мастеров своего дела с помощью идей, своих способностей и конечно же лазера для резки дерева, помогут воплотить все Ваши требования и запросы. Вместе с этим качественное оборудование с лучшими техническими характеристиками поможет создать гравировка по дереву в Киеве. Четкую, аккуратную, качественную и оригинальную.</p>

<p>Мы ждем Ваших заказов! Будем создавать уникальные и красивые предметы с помощью лазерной гравировки.</p>
</font>

&nbsp;
 </div>
</div>
<!--//-->
<a onclick=$("div[class^='spoiler_body']").hide('normal')><p class="" align="center">скрыть</p></a>
</font>

	</div>


<?php
load_more($filters_array);

if (ot_get_option('infinite_off') != 'off') { get_appear(); }
?>

    <?php 
    if($seoBlock){ ?>
      <div class="seo-block">
            <div class="container">
                <div class="seo-block-inner">
                    <?php echo $seoBlock; ?>
                </div>
                <a href="#" class="seo-block-open"><span class="open">Читать далее</span><span class="close">Скрыть</span></a>
            </div>
        </div>  
    <?php } ?>

<?php

get_footer(); ?>
<script type="text/javascript">$(document).ready(function(){ $('.spoiler_links').click(function(){  $(this).parent().children('div.spoiler_body').toggle('normal');  return false; });});</script>