��    +      t      �      �     �     �       2     )   I     s     �     �     �     �     �     �     �     �               ,     >     Q  
   i     t     �     �     �     �     �     �     �  $        &     /     4     O     j     r     ~     �     �     �     �  %   �  %     �  6  ,   �  7   �  #   1	  W   U	  W   �	  &   
     ,
  -   C
     q
  #   �
     �
  #   �
     �
  "        ;      J  "   k  "   �  1   �     �     �          "     >     O  #   k  %   �     �  C   �  #     
   7  D   B  F   �  
   �     �  <   �     .     H  /   b     �     �     �   Add New Characteristic Add or remove Characteristics All Characteristics Check this option to use Characteristics taxonomy. Choose from the most used Characteristics Clear all filters Default Edit Characteristics Filter Preset Manager Filter Settings Filter selected Filter terms In stock only New Characteristic Name Newness No Characteristics found No More Products! No products found! Popular Characteristics Popularity Price: high to low Price: low to high Product Availability Product Name Product keywords Review Count Search Characteristics Search Products Separate Characteristics with commas Settings Show Show only products on sale Show out of stock products Showing Showing all Showing the single result Special Search Filter Mode Terms Search Field Mode Update Characteristics results taxonomy general nameCharacteristics taxonomy singular nameCharacteristic Project-Id-Version: WooCommerce Product Filter
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/prdctfltr
POT-Creation-Date: 2018-03-31 15:20:07+00:00
PO-Revision-Date: 2019-09-18 10:34+0000
Last-Translator: Сережа <archie@8.marketing>
Language-Team: Русский
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_x:1,2c
X-Poedit-Basepath: .
X-Generator: Loco https://localise.biz/
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: ..
X-Loco-Version: 2.3.0; wp-4.9.11 Добавить новое свойство Добавить или удалить свойства Все характеристики Выберите одно из наиболее используемых свойств Выберите одно из наиболее используемых свойств Очистить все фильтры Стандартный Изменить характеристики Выбранный фильтр Условия фильтрации Выбранный фильтр Условия фильтрации Только в наличии Новое имя свойства Новизне Ничего не найдено Нет больше товаров Товаров не найдено Популярные характеристики Популярности Цене (Max-Min) Цене (Min-Max) Товар доступен Названию Ключевые слова Количеству отзывов Поиск характеристик Поиск товаров Отдельные характеристики с запятыми Условия фильтрации Показ Показать только товары на распродаже Показать товары, которых нет в наличии Показ Показать все Показать единственный результат Фильтр поиска Фильтр поиска Обновление характеристик Результаты Свойства Свойства 