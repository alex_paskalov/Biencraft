=== Page Builder by Mallnin ===
Tags: page builder, responsive, widget, widgets, builder, page, admin, gallery, content, cms, pages, post, css, layout, grid
Requires at least: 4.2
Tested up to: 4.5.2
Stable tag: 2.1.0
Build time: 2016-07-04T11:29:25+02:00
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Contributors: themeworm

Build responsive page layouts using the widgets you know and love using this simple drag and drop page builder.