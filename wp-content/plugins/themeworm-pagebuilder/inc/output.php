<?php
/*
Simple Page Builder - deBuilder v 3.0
http://themeforest.net/user/mallini
*/

/* ----------------------------------------------------- */
/* Load settings */
/* ----------------------------------------------------- */

function builder_settings( $type = false ) {
	global $structure;
	$buttons = $empty_xml = '';

	if ( is_array( $structure ) ) {
		foreach ( $structure as $k => $v ) {
			$buttons .= '<input type="button" id="add_item" class="button button-primary button-left" data-type="' . $v['type'] . '" value="' . $v['title'] . '" />';
			$empty_xml .= "case '" . $v['type'] . "': emptyElement = '<item id=\"'+ xmlRandom +'\" type=\"" . $v['type'] . "\"><title>" . $v['title'] . "</title><fields>";

			foreach ( $v['fields'] as $key => $val ) {
				if ( is_array( $val ) ) {
					$empty_xml .= '<field';
					foreach ( $val as $key2 => $val2 ) {
						$empty_xml .= ' ' . $key2 . '="' . $val2 . '"';
					}
					$textarea = in_array( 'textarea', $val ) ? '<![CDATA[]]>' : '';
					$empty_xml .= '>' . $textarea . '</field>';
				}
			}
			$empty_xml .= "</fields></item>';break;";
		}
	}

	if ( $type == 'buttons' ) {
		echo $buttons;
	} else {
		echo $empty_xml;
	}
}

/* ----------------------------------------------------- */
/* Translations */
/* ----------------------------------------------------- */

function pb_get_translation($word) {
	global $translate;
	if ( is_array( $translate ) ) {
		echo $string = ($translate[$word]) ? esc_attr($translate[$word]) : '';
	}
}

/* ----------------------------------------------------- */
/* Load metabox */
/* ----------------------------------------------------- */

function pb_add_meta_box() {
	$screens = array( 'post', 'page', 'portfolio' );
	add_meta_box( 'pbmb', 'Page Builder', 'pb_meta_box', $screens, 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'pb_add_meta_box' );

function pb_meta_box( $post ) {
	wp_nonce_field( 'pb_save_meta_box', 'pb_meta_box_nonce' );
	$value = get_post_meta( $post->ID, 'short_input_value', true ); ?>

	<div class="pb-wrapper">
		<form method="post" id="form-settings" action="">
			<div class="pb-header">
				<div class="pb-buttons">
					<?php builder_settings( 'buttons' ); ?>
					<a class="button button-primary button-right pb-save" data-saveclose="<?php pb_get_translation('button_saveandclose'); ?>" data-saving=" <?php pb_get_translation('button_saving'); ?>" data-save=" <?php pb_get_translation('button_save'); ?>" href="#"> <?php pb_get_translation('button_save'); ?></a>
					<input type="button" id="show_editor" class="button button-right" data-hide="<?php pb_get_translation('button_hideeditor'); ?>" data-show="<?php pb_get_translation('button_showeditor'); ?>" value="<?php pb_get_translation('button_showeditor'); ?>" />
				</div>

				<div class="pb-editor">
					<div class="pb-editor-inner">
						<?php wp_editor( '', 'build_text', array( 'textarea_name' => 'build_text', 'editor_height' => 280 ) ); ?>
					</div>
				</div>
			</div>

			<div class="pb-content">
				<div class="pb-items"></div>
				<input type="hidden" name="short_input" id="short_input" value="" />
				<input type="hidden" id="postId" value="<?php echo get_the_ID(); ?>" />
			</div>
		</form>
	</div>

	<script type="text/javascript">
		var images = '<?php if ( function_exists( 'themeworm_page_builder' ) ) { themeworm_page_builder( 'Gallery' ); } ?>';
		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';

		function getEmptyXML( elementType ) {
			var xmlRandom = Math.floor( Math.random() * 1000 ),
				emptyElement = '';

			switch ( elementType ) {
				<?php builder_settings(); ?>
			}
			return emptyElement;
		}
	</script>

	<?php if ( $value ) { ?>
		<script id="pb_xml" type="text/xmldata">
			<?php $dom = new DOMDocument;
			$dom->preserveWhiteSpace = FALSE;
			$dom->loadXML( $value );
			$dom->formatOutput = TRUE;
			foreach ( $dom->childNodes as $node ) {
				echo $dom->saveXML( $node );
			} ?>
		</script>
	<?php }
}

function pb_save_meta_box( $post_id ) {
	if ( ! isset( $_POST['pb_meta_box_nonce'] ) ) {
		return;
	}
	if ( ! wp_verify_nonce( $_POST['pb_meta_box_nonce'], 'pb_save_meta_box' ) ) {
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}
	if ( ! isset( $_POST['short_input'] ) ) {
		return;
	}
	$pb_data = $_POST['short_input'];
	update_post_meta( $post_id, 'short_input_value', $pb_data );
}
add_action( 'save_post', 'pb_save_meta_box' );

/* ----------------------------------------------------- */
/* Page Builder v3.0 */
/* ----------------------------------------------------- */

function themeworm_page_builder( $get_element = false ) {
  $xml_array = get_post_meta( get_the_ID(), 'short_input_value', true );
  $xml = ( $xml_array ) ? simplexml_load_string( $xml_array ) : '';

  if ($xml && function_exists('builder_output')) {
    builder_output($xml, $get_element);
  } else { echo ''; }
}

function getVar($elem) {
  for($i = 0; $i < count($elem->fields->children()); $i++) {
    $variable[ (string) $elem->fields->field[$i]['name'] ] = (string) $elem->fields->field[$i];
  }
  return $variable;
}

function ajax_form() {
  $fieldvalue = $_POST['fieldvalue'];
  $postid = $_POST['postid'];
  update_post_meta($postid, 'short_input_value', $fieldvalue);
  wp_die($fieldname = '', $fieldvalue = '', $postid = '');
}

add_action( 'wp_ajax_ajax_form', 'ajax_form' );
