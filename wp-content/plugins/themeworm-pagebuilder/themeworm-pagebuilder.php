<?php

/*
Plugin Name: Page Builder by Mallini
Plugin URI: https://themeforest.net/item/pinecone-creative-portfolio-and-blog-for-agency/13200056
Description: A simple wordpress Page builder for Pinecone theme
Author: Mallinidesign
Author URI: http://themeforest.net/user/mallini
Version: 3.0
Text Domain: themeworm
License: GPLv2
*/

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) exit();

/**
 * Plugin only works in WordPress 4.4 or later.
 */
register_activation_hook( __FILE__, 'pb_install' );

function pb_install() {
	if ( version_compare( get_bloginfo( 'version' ), '4.4', '<' ) ) {
		wp_die( 'This plugin requires WordPress version 4.4 or higher.' );
	}
}

/**
 * Enqueues scripts and styles.
 */
function pb_scripts() {
	wp_enqueue_style( 'pb-style', plugin_dir_url( __FILE__ ) . 'css/builder.css', false, '1.0' );
	wp_enqueue_style( 'wp-color-picker');
	wp_enqueue_script( 'wp-color-picker');
	wp_enqueue_script( 'pb-functions', plugin_dir_url( __FILE__ ) . 'js/builder.js', false, '3.0' );
}
add_action( 'admin_enqueue_scripts', 'pb_scripts' );

/**
 * Includes.
 */
include( 'inc/output.php' );
