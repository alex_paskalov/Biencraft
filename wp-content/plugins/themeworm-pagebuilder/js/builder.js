/*
Simple Page Builder - deBuilder v 3.0
http://themeforest.net/user/mallini
*/

jQuery( document ).ready( function( $ ) {

	var fields = '';

	if ( $( '#pb_xml' ).length ) {
		var xml = $.parseXML( $( '#pb_xml' ).html() );
		var $xml = $( xml ), currName, currVal, randomId;
		$xml.find( 'item' ).each( function() {
			randomId = Math.floor( Math.random() * 1000 );
			if ( $( this ).attr( 'id' ).length > 0 ) {
				randomId = $( this ).attr( 'id' );
			}
			getItems( $( this ), $( this ).attr( 'type' ) );
		} );
		$( '.pb-items' ).append( fields );
		fields = '';
		rebuild();
	}

	function getItems( xmlItem, itemType ) {
		var oldID = xmlItem.attr( 'id' );
		randomId = Math.floor( Math.random() * 1000 );

		fields += '<div class="pb-item" data-id="' + randomId + '" rel="' + itemType + '"><div class="pb-item-header"><span class="pb-item-title">' + xmlItem.find( 'title' ).text() + '</span><span class="pb-item-title-edit">[Edit]</span><input type="text" name="item_title' + randomId + '" id="item_title' + randomId + '" class="pb-item-title-input closed" value="' + xmlItem.find( 'title' ).text() + '" /><span class="dashicons dashicons-no-alt pb-item-remove"></span><div class="plchldr"></div></div><div class="pb-item-content"><div class="pb-item-content-inner">';

// 		fields += '<div class="pb-row"><div class="pb-row-title"><label for="item_title' + randomId + '">Element Title</label></div><div class="pb-row-content"><input type="text" name="item_title' + randomId + '" id="item_title' + randomId + '" class="pb-item-title" value="' + xmlItem.find( 'title' ).text() + '" /></div></div>';

		xmlItem.find( 'field' ).each( function() {
			var fieldType = $( this ).attr( 'type' );

			currName = $( this ).attr( 'name' ) + randomId;
			currVal = $( this ).text();

			fields += '<div class="pb-row">';

			fields += '<div class="pb-row-title"><label for="' + $( this ).attr( 'name' ) + randomId + '">' + $( this ).attr( 'label' ) + '</label></div>';

			switch ( fieldType ) {
				case 'textarea':
					fields += '<div class="pb-row-content"><textarea id="' + currName + '" name="' + currName + '" rows="10">'+ $( this ).text() +'</textarea><input type="button" id="paste_editor" data-id="' + randomId + '" class="button" value="Paste from Editor" /></div>';
					break;

				case 'text':
					fields += '<div class="pb-row-content"><input type="text" name="' + currName + '" id="' + currName + '" value="' + $( this ).text() + '" /></div>';
					break;

				case 'select':
					if ( currVal === '' || currVal === 'undefined' ) {
						currVal = '&mdash; Select &mdash;'
					}
					fields += '<div class="pb-row-content">' + getDropDownListXML( currName, $( this ).attr( 'options' ), $( this ).text() ) + '</div>';
					break;

				case 'number':
					fields += '<div class="pb-row-content"><input type="number" step="1" min="1" name="' + currName + '" id="' + currName + '" value="' + $( this ).text() + '"> px</div>';
					break;

				case 'colorpicker':
					fields += '<div class="pb-row-content"><input type="text" name="pb_color[' + currName + ']" id="' + currName + '" value="' + $( this ).text() + '" /></div>';
					break;

				case 'gallery':
					var galleryButtons = '<a href="#" class="button pb-gallery-create">Create Gallery</a> <a href="#" class="button pb-gallery-remove hidden">Remove</a>';
					if ( $( this ).text().length > 0 ) {
						galleryButtons = '<a href="#" class="button pb-gallery-create">Edit Gallery</a> <a href="#" class="button pb-gallery-remove">Remove</a>';
					}
					fields += '<div class="pb-row-content"><input type="hidden" name="' + currName + '" id="' + currName + '" class="pb-gallery-input" value="' + $( this ).text() + '" />' + galleryImagesXML( oldID ) + galleryButtons + '</div>';
					break;

				case 'media':
				case 'selfhosted':
					var mediaButtons = '<a href="#" class="button pb-media-upload">+</a><a href="#" class="button pb-media-remove hidden">-</a>';
					if ( $( this ).text().length > 0 ) {
						mediaButtons = '<a href="#" class="button pb-media-upload hidden">+</a><a href="#" class="button pb-media-remove">-</a>';
					}
					fields += '<div class="pb-row-content"><input type="text" name="' + currName + '" id="' + currName + '" value="' + $( this ).text() + '" class="pb-media-input">' + mediaButtons + '</div>';
					break;
			}

			fields += '</div>';
		} );

		var saveClose = $('.pb-save').attr('data-saveclose');
		fields += '</div><div class="pb-item-footer"><input type="button" data-id="' + randomId + '" class="button button-primary pb-item-save" value="' + saveClose + '"/></div></div></div>';

		// Generate select
		function getDropDownListXML( name, optionList, selected ) {
			if (optionList === '' || optionList === 'undefined' || typeof optionList === 'undefined') {
				optionList = [10, 11, 12, 13, 14, 15, 16, 18, 20, 22, 24, 26, 30, 34, 38, 44, 50]
			} else {
				optionList = optionList.split(',')
			}

			var combo = '<select name="' + name + '" id="' + name + '">';
			var addSelected = '';
			combo += '<option>&mdash; Select &mdash;</option>';
			$.each( optionList, function( i, el ) {
				if ( el == selected ) {
					addSelected = 'selected="selected"'
				}
				combo += '<option value="' + el + '" ' + addSelected + '>' + el + "</option>";
				addSelected = '';
			} );
			combo += '</select>';
			return combo;
		}

		// Get gallery images
		function galleryImagesXML( galleryId ) {
			var imagesArray = images.split( '|' ),
				imagesList = '';

			$.each( imagesArray, function( ind, elem ) {
				if ( elem != '' ) {
					if ( elem.split( ',' )[0] == galleryId ) {
						newImagesArray = elem.split( ',' );
						$.each( newImagesArray, function ( ind2, elem2 ) {
							if ( elem2 != '' && elem2.length > 3 ) {
								imagesList += '<li><img src="' + elem2 + '" /></li>';
							}
						} );
					}
				}
			} );
			imagesList = '<ul class="pb-gallery-list">' + imagesList + '</ul>';
			return imagesList;
		}

		return fields;
	}

	// Add item
	$( '.pb-header' ).on( 'click', '#add_item', function() {
		var detectedItem = '';
		detectedItem = $.parseXML( getEmptyXML( $( this ).attr("data-type") ) ),
		$detectedItem = $( detectedItem ),
		$( '.pb-items' ).append( getItems( $detectedItem, $( this ).attr("data-type") ) );
		$( xml ).find( 'items' ).append( getEmptyXML( $( this ).attr("data-type") ) );
		$( 'input[name*="pb_color"]' ).wpColorPicker();
		pb_keydown();
		fields = '';
	} );

	// Remove item
	$( '.pb-items' ).on( 'click', '.pb-item-remove', function() {
		$( this ).parents( '.pb-item' ).remove();
		rebuild();
	} );

	// Close item
	$( ".pb-items" ).on( 'click', '.plchldr', function() {
		$( this ).parents( '.pb-item' ).find( '.pb-item-content' ).slideToggle( 'slow' );
	} );

	// Sort items
	if ( $( '.pb-items' ).length ) {
		$( '.pb-items' ).sortable( {
			forcePlaceholderSize: true,
			handle: '.plchldr',
			distance: 5,
			placeholder: 'pb-item-placeholder',
			opacity: 0.7,
			update: function( event, ui ) {
				rebuild();
			}
		} );
	}

	// Color Picker
	$( 'input[name*="pb_color"]' ).wpColorPicker();

	// Edit item title
	$( '.pb-items' ).on( 'click', '.pb-item-title, .pb-item-title-edit', function() {
		$( this ).parents( '.pb-item' ).find( '.pb-item-title-input' ).fadeIn( 10 ).focus();
	} );

	$( '.pb-items' ).on( 'focusout', '.pb-item-title-input', function() {
		$( this ).parents( '.pb-item' ).find( '.pb-item-title' ).text( $( this ).val() );
		$( this ).parents( '.pb-item' ).find( '.pb-item-title-input' ).fadeOut( 10 );
	} );

	function pb_keydown() {
		$( '.pb-item-title-input' ).keydown( function( e ) {
			var key = e.which;

			if ( 13 === key ) {
				e.preventDefault();
				$( this ).parents( '.pb-item' ).find( '.pb-item-title' ).text( $( this ).val() );
				$( this ).parents( '.pb-item' ).find( '.pb-item-title-input' ).fadeOut( 10 );
			}
			if ( 27 === key ) {
				var text = $( this ).parents( '.pb-item' ).find( '.pb-item-title' ).text();
				$( this ).parents( '.pb-item' ).find( '.pb-item-title-input' ).fadeOut( 10 ).val( text );
			}
		} );
	}
	pb_keydown();

	// Show editor button
	$( '.pb-header' ).on( 'click', '#show_editor', function() {
		var editorShow = $(this).attr('data-show'),
		editorHide = $(this).attr('data-hide');
		if ( $( '#show_editor' ).val() == editorShow ) {
			$( '#show_editor' ).val( editorHide );
		} else {
			$( '#show_editor' ).val( editorShow );
		}
		$( '.pb-editor' ).slideToggle( 'slow' );
	} );

	// Paste from editor button
	$( '.pb-items' ).on( 'click', '#paste_editor', function() {
		var getId = '#build_text' + $( this ).attr( 'data-id' );
		var tinyContent = tinymce.get( 'build_text' ).getContent();
		if ( tinyContent.length > 0 ) {
			$( getId ).val( tinyContent );
		} else {
			$( getId ).val( '' );
		}
	} );

	// Save builder button
	$( '.pb-header' ).on( 'click', '.pb-save', function( e ) {
		e.preventDefault();
		rebuild();
		saveAjax();
	} );

	// Save item button
	$( '.pb-items' ).on( 'click', '.pb-item-save', function() {
		$( this ).parents( '.pb-item' ).find( '.pb-item-content' ).slideToggle( 'slow' );
		rebuild();
		saveAjax();
	} );

	// Ajax saving
	function saveAjax() {
		var postId = document.getElementById( 'postId' ).value,
		buttonSave = $('.pb-save').attr('data-save'),
		buttonSaving = $('.pb-save').attr('data-saving');
		jQuery.ajax( {
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'ajax_form',
				fieldvalue: '<items>' + rebuild() + '</items>',
				postid: postId
			},
			beforeSend: function() {
				$( '.pb-save' ).addClass( 'pb-saving' ).text( buttonSaving );
			},
			success: function() {
				setTimeout( function() {
					$( '.pb-save' ).removeClass( 'pb-saving' ).text( buttonSave );
				}, 1000 );
			},
			error: function() {
				$( '.pb-save' ).val( 'Error!' );
			}
		} );
	}

	// Rebuild new XML
	function rebuild() {
		var rebuildEmpty = '',
			newXML = '';

		$( '.pb-items' ).children( '.pb-item' ).each( function() {
			rebuildEmpty = getEmptyXML( $( this ).attr( 'rel' ) );
			var curID = $( this ).attr( 'data-id' ),
				curTitle = $( this ).attr( 'rel' ),
				curOptions = '';

			if ( $( this ).find( '.pb-item-title-input' ).val().length > 0 ) {
				curTitle = $( this ).find( '.pb-item-title-input' ).val();
			}

			newXML += '<item id="' + curID + '" type="' + $( this ).attr( 'rel' ) + '"><title>' + curTitle + '</title><fields>';

			$( rebuildEmpty ).find( 'field' ).each( function() {
				curOptions = $( this ).attr( 'options' );
				if ( typeof curOptions === 'undefined' ) {
					curOptions = '';
				}
				newXML += '<field name="' + $( this ).attr( 'name' ) + '" label="' + $( this ).attr( 'label' ) + '" options="' + curOptions + '" type="' + $( this ).attr( 'type' ) + '">' + xmlValue( $( this ).attr( 'name' ) ) + '</field>';
			} );

			newXML += '</fields></item>';

			function xmlValue( nameValue ) {
				var newXMLValue = $( '.pb-item' ).find( '#' + nameValue + curID ).val();
				if ( nameValue == 'build_text' ) {
					newXMLValue = '<![CDATA[' + newXMLValue + ']]>';
				}
				return newXMLValue;
			}
		} );

// 		$( 'input[name*="pb_color"]' ).wpColorPicker();
		$( '.pb-content' ).find( '#short_input' ).val( '<items>' + newXML + '</items>' );
		return newXML;
	}
} );

// Gallery
jQuery( document ).ready( function( $ ) {
	pb_gallery = {
		frame: function( elm ) {
			var selection = this.select( elm );

			this._frame = wp.media( {
				id: 'pb-gallery-frame',
				frame: 'post',
				state: 'gallery',
				title: wp.media.view.l10n.editGalleryTitle,
				editing: true,
				multiple: true,
				selection: selection
			} );
			this._frame.on( 'open', function() {
				var ids = $( elm ).parents( '.pb-row-content' ).children( '.pb-gallery-input' ).val();
				if ( ids ) {
					pb_gallery._frame.setState( 'gallery-edit' );
        		}
			} );
			this._frame.on( 'update', function() {
				var controller = pb_gallery._frame.states.get( 'gallery-edit' ),
					library = controller.get( 'library' ),
					ids = library.pluck( 'id' ),
					parent = $(elm).parents( '.pb-row-content' ),
					input = parent.children( '.pb-gallery-input' ),
					shortcode = wp.media.gallery.shortcode( selection ).string().replace(/\"/g,"'");

				input.attr( 'value', ids );
				var imageIDArray = [];
				var imageHTML = '';
				var metadataString = '';
				imgs = pb_gallery._frame.state().get('library');
				imgs.each( function( attachment ) {
					imageIDArray.push( attachment.attributes.id );
					imageHTML += '<li><img id="' + attachment.attributes.id + '" src="' + attachment.attributes.sizes.thumbnail.url + '" /></li>';
				} );
				metadataString = imageIDArray.join( "," );
				if ( metadataString ) {
					$( elm ).parents( '.pb-row-content' ).find( '.pb-gallery-list' ).html( imageHTML );
					$( elm ).parents( '.pb-row-content' ).find( '.pb-gallery-create' ).text( 'Edit Gallery' );
					$( elm ).parents( '.pb-row-content' ).find( '.pb-gallery-remove' ).removeClass( 'hidden' );
        		}
			} );
			return this._frame;
    	},
		select: function( elm ) {
			var input = $( elm ).parents( '.pb-row-content' ).children( '.pb-gallery-input' ),
				ids = input.attr( 'value' ),
				shortcode = wp.shortcode.next( 'gallery', ( '[gallery ids=\'' + ids + '\]' ) ),
				defaultPostId = wp.media.gallery.defaults.id,
				attachments,
				selection;

			if ( ! shortcode ) {
				return;
			}

			shortcode = shortcode.shortcode;
			if ( _.isUndefined( shortcode.get( 'id' ) ) && ! _.isUndefined( defaultPostId ) ) {
				shortcode.set( 'id', defaultPostId );
			}
			if ( _.isUndefined( shortcode.get( 'ids' ) ) && ids ) {
				shortcode.set( 'ids', ids );
			}
			if ( _.isUndefined( shortcode.get( 'ids' ) ) ) {
				shortcode.set( 'ids', '0' );
			}

			attachments = wp.media.gallery.attachments( shortcode );
			selection = new wp.media.model.Selection( attachments.models, {
				props: attachments.props.toJSON(),
				multiple: true
			} );
			selection.gallery = attachments.gallery;
			selection.more().done( function() {
				selection.props.set( {
					query: false
				} );
				selection.unmirror();
				selection.props.unset( 'orderby' );
			} );
			return selection;
		},
		open: function( elm ) {
			pb_gallery.frame( elm ).open();
		},
		remove: function( elm ) {
			if ( confirm( 'Are you sure you want to delete this Gallery?' ) ) {
				$( elm ).parents( '.pb-row-content' ).children( '.pb-gallery-input' ).attr( 'value', '' );
				$( elm ).parents( '.pb-row-content' ).find( '.pb-gallery-list' ).children().remove();
				$( elm ).parents( '.pb-row-content' ).find( '.pb-gallery-create' ).text( 'Create Gallery' );
				$( elm ).addClass( 'hidden' );
			}
		}
	}

	jQuery( document).on( 'click', '.pb-gallery-remove', function( e ) {
		e.preventDefault();
		pb_gallery.remove( $( this ) );
	} );

	jQuery( document ).on( 'click', '.pb-gallery-create', function( e ) {
		e.preventDefault();
		pb_gallery.open( $( this ) );
	} );
} );

// Media upload
jQuery( document ).ready( function( $ ) {
	pb_media = {
		frame: function( elm ) {
			this._frame = wp.media( {
				title: 'Select or Upload Media',
				multiple: false
			} );
			this._frame.on( 'select', function() {
				var attachment = pb_media._frame.state().get( 'selection' ).first().toJSON();
				$( elm ).parents( '.pb-row-content' ).find( '.pb-media-input' ).attr( 'value', attachment.url );
				$( elm ).parents( '.pb-row-content' ).find( '.pb-media-upload' ).addClass( 'hidden' );
				$( elm ).parents( '.pb-row-content' ).find( '.pb-media-remove' ).removeClass( 'hidden' );
			} );
			return this._frame;
		},
		open: function( elm ) {
			pb_media.frame( elm ).open();
		},
		remove: function( elm ) {
			$( elm ).parents( '.pb-row-content' ).find( '.pb-media-input' ).attr( 'value', '' );
			$( elm ).parents( '.pb-row-content' ).find( '.pb-media-upload' ).removeClass( 'hidden' );
			$( elm ).addClass( 'hidden' );
		}
	}

	jQuery( document).on( 'click', '.pb-media-remove', function( e ) {
		e.preventDefault();
		pb_media.remove( $( this ) );
	} );

	jQuery( document ).on( 'click', '.pb-media-upload', function( e ) {
		e.preventDefault();
		pb_media.open( $( this ) );
	} );
} );
