<?php

/*
Plugin Name: Portfolio Installer
Plugin URI: http://themeforest.net/user/mallini
Description: Enable a Portfolio post type
Author: Mallinidesign
Author URI: http://themeforest.net/user/mallini
Version: 2.1.0
Text Domain: themeworm
License: GPLv2
*/

/* Copyright 2016 Mallinidesign (http://themeforest.net/user/mallini)
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();

class Portfolio_installer {

	function __construct() {
		add_action( 'init', array( $this, 'portfolio_init' ) );
	}

	function portfolio_init() {

		/* ----------------------------------------------------- */
		/* Portfolio Pages */
		/* ----------------------------------------------------- */

		$labels = array(
			'name' =>  esc_html__('Portfolio', 'themeworm'),
			'singular_name' => esc_html__('Portfolio', 'themeworm'),
			'add_new' => esc_html__('Add New', 'themeworm'),
			'add_new_item' =>  esc_html__('Add New Project', 'themeworm'),
			'edit_item' =>  esc_html__('Edit Project', 'themeworm'),
			'new_item' =>  esc_html__('New Project', 'themeworm'),
			'view_item' =>  esc_html__('View Project', 'themeworm'),
			'search_items' =>  esc_html__('Search Portfolio', 'themeworm'),
			'not_found' =>  esc_html__('No portfolio found', 'themeworm'),
			'not_found_in_trash' =>  esc_html__('No Projects found in Trash', 'themeworm'),
			'parent_item_colon' =>  esc_html__('Parent Project:', 'themeworm'),
			'menu_name' =>  esc_html__('Portfolio', 'themeworm')
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'description' => esc_html__('Display your Projects by filters', 'themeworm'),
			'supports' => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),

			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,

			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			 'rewrite' => array( 'slug' => 'portfolio'),
			'capability_type' => 'post'
		);

		register_post_type( 'portfolio', $args );

		/* ----------------------------------------------------- */
		/* Filter Taxonomy */
		/* ----------------------------------------------------- */

		$labels = array(
			'name' =>  esc_html__('Filters', 'themeworm'),
			'singular_name' =>  esc_html__('Filter', 'themeworm'),
			'search_items' =>  esc_html__('Search Filters', 'themeworm'),
			'popular_items' =>  esc_html__('Popular Filters', 'themeworm'),
			'all_items' =>  esc_html__('All Filters', 'themeworm'),
			'parent_item' =>  esc_html__('Parent Filter', 'themeworm'),
			'parent_item_colon' =>  esc_html__('Parent Filter:', 'themeworm'),
			'edit_item' =>  esc_html__('Edit Filter', 'themeworm'),
			'update_item' =>  esc_html__('Update Filter', 'themeworm'),
			'add_new_item' =>  esc_html__('Add New Filter', 'themeworm'),
			'new_item_name' =>  esc_html__('New Filter', 'themeworm'),
			'separate_items_with_commas' =>  esc_html__('Separate Filters with commas', 'themeworm'),
			'add_or_remove_items' =>  esc_html__('Add or remove Filters', 'themeworm'),
			'choose_from_most_used' =>  esc_html__('Choose from the most used Filters', 'themeworm'),
			'menu_name' =>  esc_html__('Filters', 'themeworm')
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'hierarchical' => true,
			'rewrite' => true,
			'query_var' => true
		);

		register_taxonomy( 'filters', array('portfolio'), $args );

	}

}
new Portfolio_installer();
